import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './sass/style.scss';
import Home from './containers/home';
import Login from './containers/login';
import SignUp from './containers/signUp';
import Contact from './containers/contact';
import Pricing from './containers/pricing';
import Dashboard from './containers/dashboard';

class App extends Component {
  render() {
    return (
      <Router>
        <Route path='/' exact component={Home} />
        <Route path='/contact' component={Contact} />
        <Route path='/login' component={Login} />
        <Route path='/signup' component={SignUp} />
        <Route path='/pricing' component={Pricing} />
        <Route path='/dashboard' component={Dashboard} />
      </Router>
    );
  }
}

export default App;
