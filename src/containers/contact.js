import React, { Component } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';

export default class contact extends Component {
  render() {
    return (
      <div>
        <Header />

        <p>contact section</p>
        <Footer />
      </div>
    );
  }
}
