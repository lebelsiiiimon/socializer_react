import React, { Component } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import illustration from '../assets/img/illustration.png';
import screen1 from '../assets/img/screen1.png';
import screen2 from '../assets/img/screen2.png';
import screen3 from '../assets/img/screen3.png';

export default class pricing extends Component {
  render() {
    return (
      <div>
        <Header />
        <section className='home-section--features container'>
          <div className='features-wrapper py-5'>
            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Price 1</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Excepturi, quasi.
              </p>
              <button>houloulou</button>
            </div>

            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Price 2</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Excepturi, quasi.
              </p>
              <button>houloulou</button>
            </div>

            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Price 3</h4>
              <p>
                Sint ratione, vel doloribus quis minus quos provident.
                Excepturi, quasi.
              </p>
              <button>houloulou</button>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}
