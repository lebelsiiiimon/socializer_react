import React, { Component } from 'react';
import Sidebar from '../components/Sidebar';
import Footer from '../components/Footer';

export default class dashboardInformations extends Component {
  render() {
    return (
      <main>
        <div id='mainContent'>
          <div className='title-dashboard'>
            <h2>Mes informations</h2>
          </div>

          <div className='button-group'>
            <button
              type='button'
              className='btn btn-transparent btn-border--blue'
            >
              <i className='fas fa-edit' />
              modifier mes informations
            </button>
            <button type='button' className='btn btn-green'>
              <i className='fas fa-camera-retro' />
              modifier ma photo
            </button>
          </div>

          <div className='user-info-wrapper'>
            <div className='white-card mh'>
              <div className='info-user-line'>
                <label>Nom:</label>
                <span>"hello"</span>
              </div>
              <div className='info-user-line'>
                <label>Prénom:</label>
                <span>"hello"</span>
              </div>
            </div>
            <div className='white-card mh'>
              <div className='info-user-line'>
                <label>Adresse:</label>
                <span>13 rue de la paix, 75012 Paris 12ème</span>
              </div>
              <div className='info-user-line'>
                <label>Téléphone:</label>
                <span>06 26 02 65 16</span>
              </div>
              <div className='info-user-line'>
                <label>Mail:</label>
                <span>"hello"</span>
              </div>
            </div>
          </div>

          <div className='py-5'>
            <button
              type='button'
              className='btn btn-squared btn-blue'
              //   data-toggle='modal'
              //   data-target='#modalPhoto'
            >
              <i className='fas fa-key' />
              modifier mon mot de passe
            </button>
          </div>
        </div>
      </main>
    );
  }
}
