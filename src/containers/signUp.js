import React, { Component } from 'react';
import axios from 'axios';
import HeaderSignup from '../components/HeaderSignup';
export default class signUp extends Component {
  state = {
    email: '',
    password: '',
    confirmPassword: '',
    lastName: '',
    firstName: ''
  };

  componentDidUpdate() {
    console.log('thde state', this.state);
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  createUser = e => {
    console.log('creating a posting in db');
    console.log(this.state.email);
    console.log(this.state.password);
    const {
      email,
      password,
      confirmPassword,
      lastName,
      firstName
    } = this.state;

    axios
      .post('http://localhost:3000/auth/signup', {
        email: email, //'lol@gmail.com',
        password: password, //'lollol',
        confirmPassword: confirmPassword,
        lastName: lastName,
        firstName: firstName
      })
      .then(res => {
        console.log('the res of register:', res);
      })
      .catch(err => {
        console.log(err);
      });
    e.preventDefault();
  };

  render() {
    return (
      <div className='contact'>
        <main>
          <HeaderSignup />
          <div className='register-user-section container'>
            <div className='main-title'>
              <h1>
                Inscrivez-vous sur
                <span className='color-red' />
                Socializer.
              </h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.
              </p>
            </div>

            <form className='register-user-form'>
              <div className='row'>
                <div className='col-md-6 col-xs-12'>
                  <div className='form-group'>
                    <label>Prénom</label>
                    <input
                      name='firstName'
                      type='text'
                      placeholder='Prénom'
                      className='form-control'
                      onChange={this.onChange}
                      value={this.state.firstName}
                    />
                  </div>
                </div>

                <div className='col-md-6 col-xs-12'>
                  <div className='form-group'>
                    <label>Nom</label>
                    <input
                      name='lastName'
                      type='text'
                      placeholder='Nom'
                      className='form-control'
                      onChange={this.onChange}
                      value={this.state.lastName}
                    />
                  </div>
                </div>
                <div className='col-md-6 col-xs-12'>
                  <div className='form-group'>
                    <label>Nom d'utilisateur</label>
                    <input
                      name='email'
                      type='text'
                      maxLength='180'
                      pattern='.{5,}'
                      placeholder="Nom d'utilisateur"
                      className='form-control'
                      title='Au moins 5 caractéres'
                      onChange={this.onChange}
                      value={this.state.email}
                    />
                  </div>
                </div>
                <div className='col-md-6 col-xs-12'>
                  <div className='form-group'>
                    <label>Email</label>
                    <input
                      name='email'
                      type='email'
                      placeholder='Email'
                      className='form-control'
                      onChange={this.onChange}
                      value={this.state.email}
                    />
                  </div>
                </div>
                <div className='col-md-6 col-xs-12'>
                  <div className='form-group'>
                    <label>Mot de passe</label>
                    <input
                      name='password'
                      type='password'
                      placeholder='Mot de passe'
                      className='form-control'
                      onChange={this.onChange}
                      value={this.state.password}
                    />
                    <p className='help-block'>
                      Au moins 6 caractéres, avec une majuscule, une minuscule
                      et un chiffre
                    </p>
                  </div>
                </div>
                <div className='col-md-6 col-xs-12'>
                  <div className='form-group'>
                    <label>Confirmation mot de passe</label>
                    <input
                      name='confirmPassword'
                      type='password'
                      placeholder='Confirmez votre mot de passe'
                      className='form-control'
                      onChange={this.onChange}
                      value={this.state.confirmPassword}
                    />
                  </div>
                </div>
                <div className='col-md-12 col-xs-12'>
                  <div className='cgu-wrapper'>
                    <input type='checkbox' id='cgu' name='cgu' />
                    En vous inscrivant en tant qu'utilisateur, vous confirmez
                    que vous acceptez les Conditions Générales du site et
                    déclarez être informé des obligations civiles et fiscales
                    qui incombent aux photographes exerçant une activité
                    rémunérée.
                    <a target='_blank' href='/cgu'>
                      Lire les CGU
                    </a>
                  </div>
                </div>
              </div>
              <button
                onClick={this.createUser}
                className='btn btn-rounded btn-green'
              >
                Je m'inscris
              </button>
            </form>
          </div>
        </main>
      </div>
    );
  }
}
