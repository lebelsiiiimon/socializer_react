import React, { Component } from 'react';
import Sidebar from '../components/Sidebar';
import Footer from '../components/Footer';

export default class dashboardMessagerie extends Component {
  render() {
    return (
      <main>
        <div id='mainContent'>
          <div className='title-dashboard'>
            <h2>Mes monitoring</h2>
          </div>

          <div className='py-5 home-section--features'>
            <h3 className='pb-3'>Facebook</h3>

            <a
              href
              className='mb-3 btn btn-squared btn-transparent btn-border--blue'
            >
              <i className='fab fa-facebook-f' />
              voir la page
            </a>

            <div className='features-wrapper'>
              <div className='feature-card'>
                <h4>300</h4>
                <span>Number of friends</span>
              </div>

              <div className='feature-card'>
                <h4>1 500</h4>
                <span>Number of posts</span>
              </div>

              <div className='feature-card'>
                <h4>1 500</h4>
                <span>Number of posts</span>
              </div>
            </div>
          </div>

          <div className='py-5 home-section--features'>
            <h3 className='pb-3'>Instagram</h3>

            <a
              href
              className='mb-3 btn btn-squared btn-transparent btn-border--blue'
            >
              <i className='fab fa-instagram' />
              voir la page
            </a>

            <div className='features-wrapper'>
              <div className='feature-card'>
                <h4>300</h4>
                <span>Number of followers</span>
              </div>

              <div className='feature-card'>
                <h4>1 500</h4>
                <span>Number of posts</span>
              </div>
            </div>
          </div>
        </div>
      </main>
    );
  }
}
