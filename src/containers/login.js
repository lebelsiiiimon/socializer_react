import React, { Component } from 'react';
import HeaderLogin from '../components/HeaderLogin';
import axios from 'axios';
export default class login extends Component {
  state = {
    email: '',
    password: ''
  };

  componentDidUpdate() {
    console.log('thde state', this.state);
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleLogin = event => {
    const { email, password } = this.state;

    console.log('posting in');
    console.log(email);
    console.log(password);

    axios
      .post('http://localhost:3000/auth/signin', {
        email: email, //'lol@gmail.com',
        password: password //'lollol',
      })
      .then(res => {
        console.log('the res is :', res);
        console.log(res.data);
      })
      .catch(err => {
        console.log(err);
      });
    event.preventDefault();
    this.props.history.push('/home');
  };

  render() {
    return (
      <div className='login'>
        <HeaderLogin />
        <div className='register-user-section'>
          <div className='main-title' />
          <form className='register-user-form w-75 mx-auto'>
            <div className='row'>
              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Utilisateur / mail</label>
                  <input
                    placeholder='Utilisateur ou adresse mail'
                    type='text'
                    className='form-control'
                    name='email'
                    //   onChange={email => this.setState({ email })}
                    onChange={e => this.setState({ email: e.target.value })}
                    value={this.state.email}
                  />
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-md-6 col-xs-12'>
                <div className='form-group'>
                  <label>Mot de passe</label>
                  <input
                    type='password'
                    name='password'
                    className='form-control'
                    placeholder='Mot de passe'
                    onChange={this.onChange}
                    value={this.state.password}
                  />
                </div>
              </div>
            </div>
            <div className='remember-check'>
              <input
                type='checkbox'
                id='remember_me'
                name='_remember_me'
                //     checked
              />
              <label>Se souvenir de moi</label>
            </div>
            <input
              type='hidden'
              id='fos_user_registration_form__token'
              name='fos_user_registration_form[_token]'
              value='Q0FAwlluzXShFp-gIJG8E66IMoO55LwoYPCRo12Ys_Y'
            />
            <button
              onClick={this.handleLogin}
              className='btn btn-rounded btn-green'
            >
              Connexion
            </button>
            <a className='request-link' href='request-password.php'>
              J'ai oublié mon mot de passe
            </a>
          </form>
        </div>
      </div>
    );
  }
}
