import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import Header from '../components/Header';
import Footer from '../components/Footer';
import illustration from '../assets/img/illustration.png';
import screen1 from '../assets/img/screen1.png';
import screen2 from '../assets/img/screen2.png';
import screen3 from '../assets/img/screen3.png';

class home extends Component {
  render() {
    return (
      <div>
        <Header />
        <section className='home-section--hero container'>
          <div className='left-part'>
            <div className='hero-content'>
              <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h3>
              <h1>Socializer Insight</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nihil
                perspiciatis vero perferendis enim rerum, harum neque mollitia
                consequuntur dolor aspernatur accusamus modi. Quas eveniet
                tempora omnis soluta dolorum voluptatibus praesentium!
              </p>
              <Link to='/contact' className='btn btn-rounded btn-yellow'>
                Schedule a demo
              </Link>
            </div>
          </div>

          <div className='right-part'>
            <img src={illustration} alt='img' />
          </div>
        </section>

        <section className='home-section--overview container'>
          <div className='main-title'>
            <h2>Know your followers</h2>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta
              velit commodi facilis.
            </p>
          </div>
          <div className='interface-wrapper py-5'>
            <div className='interface-item'>
              <div className='interface-thumbnail'>
                <img src={screen1} alt='img' />
              </div>
              <div className='interface-item--content'>
                <h3>Forecast</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Vitae laborum soluta porro repellat, neque quos! Nisi atque
                  odio dolorum voluptatum inventore
                </p>
              </div>
            </div>
            <div className='interface-item'>
              <div className='interface-thumbnail'>
                <img src={screen2} alt='img' />
              </div>

              <div className='interface-item--content'>
                <h3>Visualization</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Vitae laborum soluta porro repellat, neque quos! Nisi atque
                  odio dolorum voluptatum inventore
                </p>
              </div>
            </div>
            <div className='interface-item'>
              <div className='interface-thumbnail'>
                <img src={screen3} alt='img' />
              </div>

              <div className='interface-item--content'>
                <h3>Improvment</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                  Vitae laborum soluta porro repellat, neque quos! Nisi atque
                  odio dolorum voluptatum inventore
                </p>
              </div>
            </div>
          </div>
        </section>

        <section className='home-section--features container'>
          <div className='features-wrapper py-5'>
            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Feature 1</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Excepturi, quasi.
              </p>
            </div>

            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Feature 2</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Excepturi, quasi.
              </p>
            </div>

            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Feature 3</h4>
              <p>
                Sint ratione, vel doloribus quis minus quos provident.
                Excepturi, quasi.
              </p>
            </div>

            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Feature 4</h4>
              <p>
                Cupiditate dolorem, pariatur unde sequi laudantium deserunt quo
                animi sit modi iure sint ratione, vel doloribus quis minus quos
                provident.
              </p>
            </div>

            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Feature 5</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Excepturi, quasi.
              </p>
            </div>

            <div className='feature-card'>
              <div className='card-icons' />

              <h4>Feature 6</h4>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Cupiditate dolorem, pariatur unde sequi.
              </p>
            </div>
          </div>
        </section>

        <section className='home-section--demo bg-color--blue py-5'>
          <div className='main-title'>
            <h2>Want a personal Tour ?</h2>
            <p>
              Dolores modi reprehenderit, facilis nihil nihil nam iste magni
              recusandae tempora a voluptatem.
            </p>
            <div className='py-5'>
              <a href='true' className='btn btn-square btn-green'>
                Schedule a demo
              </a>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}
export default home;
