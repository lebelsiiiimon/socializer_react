import React, { Component } from 'react';

export default class Footer extends Component {
  render() {
    return (
      <footer className='footer'>
        <p>Copyright © Socializer 2019</p>
      </footer>
    );
  }
}
