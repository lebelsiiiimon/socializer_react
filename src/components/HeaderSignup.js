import React, { Component } from 'react';
import { BrowserRouter as Link, NavLink } from 'react-router-dom';
import logo from '../assets/img/logo_socializer.svg';

class HeaderSignup extends Component {
  render() {
    return (
      <header className='header'>
        <div className='header-logo'>
          <NavLink to='/' className='nav-link'>
            <img src={logo} alt='okok' />
          </NavLink>
        </div>
        <div className='login-section'>
          Déjà inscrit ?
          <NavLink
            to='/login'
            className='btn btn-rounded btn-transparent btn-border--blue'
          >
            connexion
          </NavLink>
        </div>
      </header>
    );
  }
}

export default HeaderSignup;
