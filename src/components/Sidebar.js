import React, { Component } from 'react';
import { BrowserRouter, NavLink as Link } from 'react-router-dom';

export default class Sidebar extends Component {
  render() {
    return (
      <div>
        <aside id='sideBarUser'>
          <div className='header-user'>
            <div className='logo-wrapper' />
            <div className='profil-picture no-picture'>
              <a href='/'>
                <div className='no-picture-wrapper'>
                  <i className='fas fa-upload' />
                  <p>Ajouter une photo</p>
                </div>
              </a>
            </div>
            <h3>Daniel</h3>
          </div>
          {/* menu */}
          <nav>
            <ul>
              <li>
                <Link to='/dashboard/monit' activeClassName='active'>
                  <i className='fas fa-camera-retro' />
                  monitoring
                </Link>
              </li>
              <li>
                <Link to='/dashboard/message' activeClassName='active'>
                  <i className='far fa-comments' />
                  messagerie
                </Link>
              </li>
              <li>
                <Link to='/dashboard/info' activeClassName='active'>
                  <i className='fas fa-user-circle' />
                  informations
                </Link>
              </li>
              <li>
                <Link to='/dashboard/pref' activeClassName='active'>
                  <i className='fas fa-cog' />
                  préférences
                </Link>
              </li>
            </ul>
          </nav>

          <div className='footer-user'>
            <button type='button' name='button' className='btn btn-reset'>
              <i className='fas fa-power-off' />
              déconnexion
            </button>
          </div>
        </aside>
      </div>
    );
  }
}
